create table if not exists events
(
    id           serial
        primary key,
    deadline timestamp not null,
    coefficient_1 float ,
    coefficient_2 float ,
    status       integer not null
);