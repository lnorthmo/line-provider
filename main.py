from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from routes import line
from services import line_service

app = FastAPI()


app.include_router(line.router)


@app.on_event("startup")
@repeat_every(seconds=30)
async def update_events() -> None:
    await line_service.update_events()
