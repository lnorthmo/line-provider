import enum
import decimal
from datetime import datetime
from pydantic import BaseModel
from typing import Optional


class Status(enum.Enum):
    in_process = 0
    t1_win = 1
    t2_win = 2


class EventBase(BaseModel):
    id: Optional[int] = None
    status: Optional[Status] = 0


class Event(EventBase):
    deadline: datetime
    coefficient_1: decimal.Decimal
    coefficient_2: decimal.Decimal
