from fastapi import APIRouter

from services import line_service
from schemas.line_schema import Event, EventBase

router = APIRouter()


@router.get("/get_events")
async def get_events():
    return await line_service.get_events()


@router.post("/create_event")
async def create_line_event(event: Event):
    return await line_service.create_line_event(event)


@router.get("/get_event")
async def get_event_by_id(event_id: int):
    return await line_service.get_event_by_id(event_id)


@router.post("/update_event")
async def update_event(event: EventBase):
    return await line_service.update_event(event)


@router.get('/get_finished_events')
async def get_finished_events():
    return await line_service.get_finished_events()
