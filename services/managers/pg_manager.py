import datetime

import asyncpg

from settings import PostgreSqlConfig
from schemas.line_schema import Event, EventBase


async def connect():
    conn = await asyncpg.connect(PostgreSqlConfig.DATABASE_URL)
    return conn


async def close(conn):
    await conn.close()


async def create_event(event: Event) -> int:
    cur = await connect()

    event_id = await cur.fetchval("insert into events (deadline, status, coefficient_1, coefficient_2)"
                                  " values ($1, $2, $3, $4)"
                                  "on conflict do nothing returning id",
                                  event.deadline, event.status, event.coefficient_1, event.coefficient_2)

    await close(cur)
    return event_id


async def get_events():
    cur = await connect()

    data = await cur.fetch("select * from events where deadline > $1", datetime.datetime.now())
    await close(cur)

    return data


async def get_event_by_id(event_id: int):
    cur = await connect()

    data = await cur.fetch("select * from events where id = $1", event_id)
    await close(cur)
    return data


async def update_events(result: int):
    cur = await connect()

    await cur.execute("update events set status = $1 where deadline < $2 and status = 0", result,
                      datetime.datetime.now())
    await close(cur)


async def get_finished_events():
    cur = await connect()
    deadline_max = datetime.datetime.now() - datetime.timedelta(days=7)
    data = await cur.fetch("select * from events where status != 0 and deadline >= $1", deadline_max)
    return data


async def update_event(event: EventBase):
    cur = await connect()

    data = await cur.fetchval("update events set status = $1 where id = $2 returning *", event.status.value, event.id)

    await close(cur)

    return data
