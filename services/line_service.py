import random

from schemas.line_schema import Event, EventBase
from services.managers import pg_manager


async def create_line_event(event: Event):
    event_id = await pg_manager.create_event(event)
    return {"event_id": event_id}


async def get_events():
    return await pg_manager.get_events()


async def get_event_by_id(event_id: int):
    return await pg_manager.get_event_by_id(event_id)


async def update_events():
    res = random.randrange(1, 3)
    await pg_manager.update_events(res)


async def get_finished_events():
    return await pg_manager.get_finished_events()


async def update_event(event: EventBase) -> dict:
    return await pg_manager.update_event(event)
